<?php


require("connect.php");

$limit = 250; //nombre d'enregistrement par page maximal
$desc = "";

if($_GET["sorted"] == null){
    $sorted = "article";

} else if($_GET["sorted"] == "Reference" || $_GET["sorted"] == "Referencesame"){
    $sorted = "reference_cindoc";

} else if($_GET["sorted"] == "Ville" || $_GET["sorted"] == "Villesame"){
    $sorted = "nom";

}  else {
    $sorted = $_GET["sorted"];
}

if(substr($_GET["sorted"], -4) == "same") {
    $desc = "DESC";
    if(substr($sorted, -4) == "same"){
        $sorted = substr_replace($sorted, "", -4);
    }
}



if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
if($_GET["page"] == null){
    $page=1;
}
$start_from = ($page-1) * $limit;


if($_GET["query"] == null){
    $result = pg_query($db,"
  SELECT reference_cindoc, article, sujet, nom, date 
    FROM articles_details ad
    LEFT JOIN Villes v ON v.id_ville = ad.id_ville
  ORDER BY $sorted $desc OFFSET $start_from LIMIT $limit
 ");

    $rowCount = pg_query($db, "
  SELECT COUNT(*) FROM articles_details");
} else
{

    $search = pg_escape_string($db, strtolower($_GET["query"]));

    $result = pg_query($db, "
  SELECT reference_cindoc, article, sujet, nom, date 
  FROM articles_details ad
  LEFT JOIN Villes v ON v.id_ville = ad.id_ville
  WHERE CAST(reference_cindoc AS TEXT) LIKE '%".$search."%'
   OR CAST(article AS TEXT) LIKE '%".$search."%'
   OR sujet LIKE '%".$search."%'
   OR CAST(date AS TEXT) LIKE '%".$search."%'
   OR nom LIKE '%".$search."%'
   GROUP BY reference_cindoc, article, sujet, nom, date
   ORDER BY $sorted $desc OFFSET $start_from LIMIT $limit
 ");


    $rowCount = pg_query($db, "
SELECT COUNT(*) FROM articles_details ad
  LEFT JOIN Villes v ON v.id_ville = ad.id_ville
  WHERE CAST(reference_cindoc AS TEXT) LIKE '%".$search."%'
   OR CAST(article AS TEXT) LIKE '%".$search."%'
   OR sujet LIKE '%".$search."%'
   OR CAST(date AS TEXT) LIKE '%".$search."%'
   OR nom LIKE '%".$search."%'   
");

}

if (!$result) {
    echo "Une erreur s'est produite.\n";
    exit;
}


$rows =  pg_fetch_result($rowCount, 0, 0);
$total_pages = ceil($rows/$limit);


echo "Enregistrement trouvés: ".number_format($rows, 0, ',', ' ');;
echo '<div id="pagination">';

if($page > 1){
    echo '<p id="page1"><<</p>';
}

if($page > 10) {
    echo '<p id="page' . ($page - 10) . '"><</p>';
}

if($page > 0 && $page < $total_pages+1){

    if($total_pages > 5) {
        if ($page < 3) {
            for ($i = 0; $i <= 4; $i++) {
                echo '<p id="page' . (1 + $i) . '">' . (1 + $i) . "</p>";
            }

        } else if ($page >= $total_pages - 2) {
            for ($i = -4; $i <= 0; $i++) {
                echo '<p id="page' . ($total_pages + $i) . '">' . ($total_pages + $i) . "</p>";
            }

        } else {
            for ($i = -2; $i <= 2; $i++) {
                echo '<p id="page' . ($page + $i) . '">' . ($page + $i) . "</p>";
            }
        }

    } else {
        for ($i = 1; $i <= $total_pages; $i++) {
            echo '<p id="page' . $i . '">' . $i . "</p>";
        }
    }
}

if($page < $total_pages-9){
    echo '<p id="page'.($page+10).'">></p>';
}

if($page < $total_pages-5) {
    echo '<p id="page' . ($total_pages) . '">>></p>';
}

echo "
</div>

<div id='interface_table'>
    <table>
    <thead>
        <tr id='sort_table'>
            <th>Reference</th>
            <th>Article</th>
            <th>Sujet</th>
            <th>Ville</th>
            <th>Date</th>
        </tr>
        </theah><tbody>";
while ($row = pg_fetch_row($result)) {
    echo "<tr>";
    echo "<td>" . $row[0] . "</td>";
    echo "<td>" . $row[1] . "</td>";
    echo "<td>" . ucfirst($row[2]) . "</td>";
    echo "<td>" . ucwords($row[3]) . "</td>";
    echo "<td>" . date('d.m.Y', strtotime($row[4])) . "</td>";
    echo "</tr>";
}
echo "</tbody></table></div>";

?>