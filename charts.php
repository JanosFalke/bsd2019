<?php
/**
 * Created by PhpStorm.
 * User: janosfalke
 * Date: 2019-01-28
 * Time: 21:35
 */
/**
 * Projet BSD 2019
 * Falke Janos & Adalbert Michael
 */
require("connect.php");



?>
<html>
<head>
    <meta charset="utf-8">
    <title>Projet Base de Données 2019</title>
    <link rel="stylesheet" href="css/main.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
    <script type = "text/javascript" src = "js/charts.js" ></script>
</head>
<body>
<header>
    <h1>
        Projet Base de Données 2019
    </h1>
</header>
<main>
    <nav class="topnav">
        <a href="index.php">Home</a>
        <a class="active" href="#">Charts</a>
    </nav>

    <div class="headerChart">
        <h3>Top 25 Villes : Nombre d'enregistrements de clichés</h3>
    </div>
    <div class="chart_container">
        <canvas id="graphCanvas"></canvas>
    </div>


</main>



</body>
</html>
