<?php
/**
 * Created by PhpStorm.
 * User: janosfalke
 * Date: 2019-01-28
 * Time: 21:35
 */
/**
 * Projet BSD 2019
 * Falke Janos & Adalbert Michael
 */
require("connect.php");



?>
<html>
<head>
    <meta charset="utf-8">
    <title>Projet Base de Données 2019</title>
    <link rel="stylesheet" href="css/main.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type = "text/javascript" src = "js/select.js" ></script>
</head>
<body>
<header>
    <h1>
        Projet Base de Données 2019
    </h1>
</header>
<main>
    <nav class="topnav">
        <a class="active" href="#">Home</a>
        <a href="charts.php">Charts</a>
    </nav>


    <span class="search_box">Search</span>
    <input type="text" name="search_text" id="search_text" placeholder="Chercher..." class="form-control" />

    <div id="target-content" >loading...</div>


</main>



</body>
</html>
