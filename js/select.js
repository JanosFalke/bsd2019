var search_value = null;
var page = null;
var sorted = "Article";

$(document).ready(function() {

    load_data(null,null, sorted);

    $(document).on('click', '#pagination p', function(){
        var pageName = $(this).attr("id");
        console.log(search_value);
        page = pageName.substring(4);
        load_data(search_value,page,sorted);
    });

    $(document).on('click', '#sort_table th', function(){
        var column_name = $(this).html();
        if (sorted == column_name){

            sorted = sorted+"same";
        } else {
            sorted = column_name;
        }
        load_data(search_value,page,sorted);
    });


    $('#search_text').keyup(delay(function (e) {
        var search = $(this).val();
        search_value = search;
        if (search != '') {
            load_data(search, null,sorted);
        } else {
            load_data(null,null,sorted);
        }
    }, 400));


});

function delay(callback, ms) {
    var timer = 0;
    return function() {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            callback.apply(context, args);
        }, ms || 0);
    };
}


function load_data(query,page, sorted)
{
    $.ajax({
        url:"getarticle.php",
        method:"GET",
        data:{query:query, page:page, sorted:sorted},
        success:function(data)
        {
            $('#target-content').html(data);
        }
    });
}

