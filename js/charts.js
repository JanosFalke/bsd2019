$(document).ready(function() {

    $.ajax({
        url : "data.php",
        type : "GET",
        success : function(data){
            console.log(JSON.parse(data));
            data = JSON.parse(data);

            var nb_article = [];
            var nom = [];
            var enregistrement = [];

            for (var i in data) {
                nb_article.push(data[i][0]);
                nom.push(data[i][1].charAt(0).toUpperCase() + data[i][1].slice(1));
                enregistrement.push(data[i][2]);
            }

            var chartdata = {
                labels: nom,
                datasets: [
                    {
                        label: 'Clichés par ville',
                        backgroundColor: '#49e2ff',
                        borderColor: '#46d5f1',
                        hoverBackgroundColor: '#46d5f1',
                        hoverBorderColor: '#46d5f1',
                        data: enregistrement
                    },
                    {
                        label: 'Articles par ville',
                        backgroundColor: '#12aa12',
                        borderColor: '#12aa12',
                        hoverBackgroundColor: '#12aa12',
                        hoverBorderColor: '#12aa12',
                        data: nb_article
                    }
                ]
            };

            var graphTarget = $("#graphCanvas");

            var barGraph = new Chart(graphTarget, {
                type: 'bar',
                data: chartdata,
                options: {
                    maintainAspectRatio: false,
                        scaleShowValues: true,
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }],
                            xAxes: [{
                                ticks: {
                                    autoSkip: false
                                }
                            }]
                        },
                    legend: {
                        onClick: null
                    }
                    },
            });

        },
        error : function(data) {
            console.log(data);
        }
    });

});